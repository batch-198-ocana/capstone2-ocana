const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

router.post('/',userControllers.registerUser);

router.get('/getUserDetails',verify,userControllers.getUserDetails);

router.post('/login',userControllers.loginUser);

router.post('/order',verify,userControllers.order);

router.put('/updateAdmin/:id',verify,verifyAdmin,userControllers.updateAdmin);

router.get('/retrieveOrders',verify,userControllers.retrieveOrders)

router.get('/displayOrderedProducts/:id',verify,userControllers.displayOrderedProducts)

module.exports = router;

