const User = require("../models/User");
const Product = require("../models/Product")

const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (req,res) =>{
    const hashedPw = bcrypt.hashSync(req.body.password,10);
    let newUser = new User({
        firstName:req.body.firstName,
        lastName:req.body.lastName,
        email:req.body.email,
        password:hashedPw,
        mobileNo:req.body.mobileNo
    })
    newUser.save()
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.getUserDetails = (req,res) =>{
    User.findById(req.user.id)
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.loginUser = (req,res) =>{
    User.findOne({email:req.body.email})
    .then(foundUser=>{
        if(foundUser === null){
            return res.send({message:"No User Found."});
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
            if(isPasswordCorrect){
                return res.send({accessToken:auth.createAccessToken(foundUser)});
            } else {
                return res.send({message:"Username and/or password is incorrect."});
            }
        }
    })
    .catch(err=>res.send(err))
}

module.exports.order = async (req,res) =>{
    if(req.user.isAdmin){
        return res.send({message:"Action Forbidden"});
    } else {
        let isUserUpdated = await User.findById(req.user.id).then(user=>{
            let newOrder = {
                totalAmount:req.body.totalAmount
            }
            user.orders.push(newOrder);
            for(let i = 0; i < req.body.products.length; i++){
                let orderProduct = {
                    productId:req.body.products[i].productId,
                    quantity:req.body.products[i].quantity
                }
                let orders = {
                    orderId:user.orders[user.orders.length - 1].id,
                    userId:req.user.id,
                    quantity:req.body.products[i].quantity
                }
                user.orders[user.orders.length - 1].products.push(orderProduct);
                Product.findById(req.body.products[i].productId)
                .then(product=>{
                        product.orders.push(orders);
                        product.save();
                })
            }
            return user.save().then(user=>true).catch(err=>err.message); 
        })
        if(isUserUpdated !== true){
            return res.send({message:isUserUpdated});
        }

        if(isUserUpdated){
            return res.send({message:"Order Successful"});
        }
    }
}

module.exports.updateAdmin = (req,res) =>{
    let update = {
        isAdmin:true
    }
    User.findByIdAndUpdate(req.params.id,update,{new:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err));
}

module.exports.retrieveOrders = (req,res) =>{
    User.findById(req.user.id)
    .then(result=>{
        res.send(result.orders)
    })
    .catch(err=>res.send(err))
}

module.exports.displayOrderedProducts = (req,res) =>{
    if(req.user.isAdmin){
        return res.send({message:"Action Forbidden"});
    } else {
        User.findById(req.user.id)
        .then(result=>{
            let arr = [];
            for(let i = 0;i < result.orders.length; i++){
                if(result.orders[i].id === req.params.id){
                    res.send(result.orders[i])
                }
            }
        })
        .catch(err=>res.send(err))
    }
}