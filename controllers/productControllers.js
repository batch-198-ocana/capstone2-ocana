const Product = require("../models/Product");
const User = require("../models/User")

module.exports.getActiveProducts = (req,res) =>{
    Product.find({isActive:true},{orders:0})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.retrieveProduct = (req,res) =>{
    Product.findById(req.params.id,{orders:0})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.addProduct = (req,res) =>{
    let newProduct = new Product({
        name:req.body.name,
        description:req.body.description,
        price:req.body.price
    })
    newProduct.save()
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.updateProduct = (req,res) =>{
    let update = {
        name:req.body.name,
        description:req.body.description,
        price:req.body.price
    }
    Product.findByIdAndUpdate(req.params.id,update,{new:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.archiveProduct = (req,res) =>{
    let update = {
        isActive:false
    }
    Product.findByIdAndUpdate(req.params.id,update,{new:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.retrieveAllOrders = (req,res) =>{
    User.find({$and:[{orders:{$ne:[]}},{isAdmin:{$ne:true}}]},{orders:1})
    .then(result=>{
        if(result.orders !== []){
            res.send(result);
        }
    })
    .catch(err=>res.send(err))
}

module.exports.activateProduct = (req,res) =>{
    let update = {
        isActive:true
    }
    Product.findByIdAndUpdate(req.params.id,update,{new:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}